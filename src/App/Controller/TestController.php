<?php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("", name="app")
 */
class TestController
{
    /**
     * @Route("/", name="")
     */
    public function index()
    {
        return new Response(
            '<html><body>xxx</body></html>'
        );
    }
}