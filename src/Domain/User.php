<?php
namespace Domain;

use Domain\Exception\User\InvalidDataException;
use Domain\Task;

class User {
    private $uuid;
    private $name;
    private $tasks;

    public function __construct(string $name)
    {
        if($name === "")
        {
            throw new InvalidDataException('Name cannot be empty');
        }
        $this->uuid = uniqid('user_', true);
        $this->name = $name;
        $this->tasks = [];
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function createTaskWithName(string $name)
    {
        $task = new Task($this, $name);
        $this->tasks[] = $task;
        return $task;
    }

    public function getTasks(): array
    {
        return $this->tasks;
    }
}