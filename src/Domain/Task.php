<?php
namespace Domain;

use Domain\Exception\Task\InvalidDataException;

class Task {
    private $uuid;
    private $user;
    private $name;

    public function __construct(User $user, string $name)
    {
        $this->changeName($name);   
        
        $this->uuid = uniqid('user_', true);
        $this->user = $user;
         
    }

    public function changeName(string $name)
    {
        if($name === "")
        {
            throw new InvalidDataException("Name cannot be empty");
        }

        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }

}