<?php
namespace Domain\Repository;

use Domain\User;

interface UserRepositoryInterface {
    public function find(string $uuid): User;
}