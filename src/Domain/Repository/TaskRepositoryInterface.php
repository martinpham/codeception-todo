<?php
namespace Domain\Repository;

use Domain\Task;
use Domain\User;

interface TaskRepositoryInterface {
    public function find(string $uuid): Task;
    public function findAllByUser(User $user): array;

    public function save(Task $task);

    public function remove(Task $task);
}