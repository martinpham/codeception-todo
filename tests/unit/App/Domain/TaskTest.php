<?php namespace App\Tests\Domain;

use Domain\User;
use Domain\Task;
use Domain\Exception\Task\InvalidDataException;

class TaskTest extends \Codeception\Test\Unit
{
    /**
     * @var \App\Tests\UnitTester
     */
    protected $tester;

    protected $user;
    
    protected function _before()
    {
        $this->user = $this->make('Domain\User');
    }

    protected function _after()
    {

    }

    // tests
    public function testCreateTask()
    {
        // name not empty
        $this->tester->expectException(
            new InvalidDataException('Name cannot be empty'), 
            function() {
                $task = new Task($this->user, "");
            }
        );

        $task = new Task($this->user, "buy milk");
        $this->tester->assertEquals("buy milk", $task->getName());
    }

    public function testChangeName()
    {
        $task = new Task($this->user, "buy milk");

        // name not empty
        $this->tester->expectException(
            new InvalidDataException('Name cannot be empty'), 
            function() use($task) {
                $task->changeName("");
            }
        );

        $task->changeName("buy sugar");
        $this->tester->assertEquals("buy sugar", $task->getName());
    }
}