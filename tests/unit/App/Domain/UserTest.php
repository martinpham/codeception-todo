<?php namespace App\Tests\Domain;

use Domain\User;
use Domain\Exception\User\InvalidDataException;
use Domain\Exception\Task\InvalidDataException as InvalidTaskDataException;

class UserTest extends \Codeception\Test\Unit
{
    /**
     * @var \App\Tests\UnitTester
     */
    protected $tester;
    
    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testCreateUser()
    {
        // name not empty
        $this->tester->expectException(
            new InvalidDataException('Name cannot be empty'), 
            function() {
                $user = new User("");
            }
        );

        $user = new User("test");
        $this->tester->assertEquals("test", $user->getName());
    }
    
    public function testUserCreatesTask()
    {
        $user = new User("test");

        // name not empty
        $this->tester->expectException(
            new InvalidTaskDataException('Name cannot be empty'), 
            function() use($user) {
                $user->createTaskWithName("");
            }
        );

        $task = $user->createTaskWithName("buy milk");
        $this->tester->assertEquals("buy milk", $task->getName());
        $this->tester->assertContains($task, $user->getTasks());
    }
    
}